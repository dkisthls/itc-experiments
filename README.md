# HLS-Generated Experiments for IT&C #

The Operations Tools generated Experiments and OPs will not be available in
time for IT&C activities which in some cases depend on OPs being present.
Instead HLS is responsible for generating the OPs necessary to complete IT&C.
This repository contains that work.  Specifically it contains the Experiment
Lists, Experiments, OPs, OP Scripts, OP Params (including TCS), IPs and DSPs
necessary to complete IT&C activities.  There are typically hand generated and
are expensive to maintain.  As a result these are quick and dirty OPs that do
the bare minimum to satisfy the IT&C need.  If there are of use to the Ops
Tools team thats great and they are welcome to them but the focus is NOT on OPs
for Operations.

### Using this repo ###

- Clone the repo
- `cd` to the top level of the repo
- Install the Experiments:
  - Use case 1: build on local machine and install elsewhere:
    - Run the `makeRunScript` shell script (make sure makeself is installed
      (apt/yum))
    - send the generated op_install.run to the summit system (`scp`)
    - run op_install.run on a machine with an ASDT and the OCS installed
    - one liner: `./makeRunScript && scp op_install.run <host>:~/ && ssh <host>
      ./op_install.run --nox11 -- --atst=/opt/ats`
  - Use case 2: build installer and run from the same machine
    - `./makeRunScript --run`
- On the OCS Obs Run GUI add the appropriate experiment list

### Structure ###

The structure of this repos rughly follows the *experiment* structure:
- Experiment List
  - Experiment 1
    - OP 1-a
    - OP 1-b
  - Experiment 2
  - ...

Ignoring the top level bash directory (which contains the script necessary to
perform the DB load) that yields:
- exp-list (i.e. sim-10) contains all of the experiments and sub-parts that make
  up the given experiment list
  - exp-list/list.json: The list of experiments (it would be cool if this could
    be generated but alass it cannot).
  - exp-list/other: files that are pertinent to all experiments within the list.
    In the case of sim-10 this includes the pol cal OP script (which is shared
    across all Experiments/OPs as well as the target list
  - exp-list/exp: files relevant to all OPs within the experiment
  - exp-list/exp/op:

### Contacts? ###

* John Hubbard jhubbard@nso.edu
* Andy Ferayorni andyf@nso.edu
