#!/bin/bash

LOAD_SCRIPTS=true
LOAD_IPS=true
LOAD_DSPS=true
LOAD_JSONS=true

DEBUG=false
ERRORS=false

warn() {
  echo "!!! $@" 1>&2
}

note() {
  echo "$@"
}

debug() {
  if $DEBUG; then echo "$@"; fi
}

parseArgs() {
  for arg in $@; do
    case $arg in
      --help)
        help
        exit 0
        ;;
      --debug)
        debug=true
        ;;
      --atst=*)
        export ATST=${arg#--atst=}
        note "setting ATST to $ATST"
        ;;
    esac
  done
}

help() {
  note "help comming; read the script for now"
}

#fileType in json ip dsp py sh; do 

loadWithReader() {
  dir=$1; shift
  reader=$1; shift
  if [[ $# -eq 0 ]]; then
    what=$dir
  else
    what=$1
    shift
  fi


  files=$(shopt -s nullglob dotglob; echo $dir/*)

  if (( ${#files} )) ; then
    note "Loading ${what}s"
    $ATST/bin/$reader ${files}
    if [[ ! $? ]]; then
      warn "${what} load had errors"
      ERRORS=true
    fi
    note
  else
    note "No $what files found"
  fi
}

loadJson() {
  files=$(shopt -s nullglob dotglob; echo json/*)
  if (( ${#files} )) ; then
    note "Loading json messages"
    $ATST/bin/ExprListReader --dir=json --verbose
    if [[ ! $? ]]; then
      warn "json load had errors"
      ERRORS=true
    fi
    note
  else
    note "No $what files found"
  fi

}

parseArgs $@


if [[ -z $ATST ]]; then
  warn "\$ATST is undefined.  Export it or re-run passing \"-- --atst=..." >&2
  exit 1
fi

debug "\$ATST=$ATST"
note

loadWithReader dsp DSPReader DSP
loadWithReader py ScriptReader script
loadWithReader ip ParamSetReader IP
loadJson


if $ERRORS ; then 
  warn "Errors encountered see above"
  exit 1
else
  note "Success"
fi


