##
# Header parsed by the ScriptReader
# category    = atst.ocs.op
# name        = polcal
# id          = sim-10-polcal
# language    = jython
# description = The sim 10 pol cal scirpt
# view        = no-view


# OP Parameters required/supported by this pol cal script
# fieldStop: (required)
#   -- value either fstop28 or fstop5 (passed to gos as l0 optic)
# polarizer: (required)
#   -- value one of fsilpol or saphpol (passed to gos as l2 optic)
# retarder: (required)
#   -- value one of sio2sar, sio2oc, mgfl2sa (passed to gos as l1 optic)
# tableName: (supported; default=short)
#   -- value one of short, long (used to generate ret/pol configs)
# useLamp: (suported: default=False)
#   -- value one of true, false (used to specify a lamp pol cal vs
#      a proper solar pol cal)
#


def doWork():
    # if useLamp isn't present this will be None which has fails the truthyness
    # test (and thus looks like false which is the default we want).  If useLamp
    # is present we want to use whatever value it has.  Either way we don't need
    # to worry abut missing/malformed useLamp
    useLamp = params.getBoolean('useLamp')

    if useLamp:
        checkWithOperator(
            'The GOS lamp will be used for this OP. ' +
            'Ensure that the the M1 cover is in the closed position, ' +
            'and that it remains closed for the duration of this OP')

    icsTask = makeTaskGroup()  # same task for every target
    # probably 4 targets (sun center @ 4 table angles)
    if targets:  # false if targets is None or targets is empty
        for targetIndex, target in enumerate(targets):
            TCSexecuteAndAdjust(TCSextract(target))
            runConfigForCurrentTarget(1 + targetIndex, icsTask, useLamp)
    else:  # hmmm no targets so assume operator will change table angle
        checkWithOperator(
            'No targets found.  Operator in charge of setting target/table angle')
        runConfigForCurrentTarget(0, icsTask, useLamp)


def runConfigForCurrentTarget(targetIndex, icsTask, useLamp):
    gosConfigs = buildGosConfigs(useLamp)
    for row, gosCfg in enumerate(gosConfigs):
        ICSsetup(icsTask)
        report('...at row ' + str(row + 1) + '/' + str(len(gosConfigs))
               + ' of target ' + str(targetIndex))
        TCSexecuteAndWait(gosCfg)
        ICSexecuteAndWait(icsTask)
# end doWork()


def buildGosConfigs(useLamp):
    # table contains the combination of polarizer angle and retarder angle needed
    # for one 'step'.  If the retarder angle is None that indicates that it should
    # not be in the beam

    tableName = params.getString('tableName')
    if not tableName:
        tableName = 'short'

    if 'long' == tableName:
        table = [
            (0,   None),  (45,  None),  (90,  None),  (135, None),
            (135, 0),     (135, 45),    (135, 90),    (180, 135),
            (180, 180),   (180, 225),   (225, 270),   (225, 315),
            (225, 0),     (270, 45),    (270, 90),    (270, 135)
        ]
    elif 'short' == tableName:
        table = [
            (0,  None),  (60, None),  (120, None),  (0,  0),
            (0,  60),    (0,  120),   (45,  0),     (45, 30),
            (45, 90),    (45, 150)
        ]
    elif 'retardance' == tableName:
        table = [
            (0, None), (60, None), (120, None),
            (120, 0), (60, 0), (0, 0),
            (0, 60), (60, 60), (120, 60),
            (120, 120), (60, 120), (0, 120)]

    configs = list()
    configs.append(darkConfig(useLamp))
    configs.append(clearConfig(useLamp))
    for p, r, in table:
        configs.append(polConfig(p, r))
    configs.append(clearConfig(useLamp))
    configs.append(darkConfig(useLamp))
    return configs
# end buildGosConfigs()


def mkGosConfig():
    global config
    cfg = Configuration(config)
    cfg.setFQPrefix('atst.tcs.pac.gos')
    return cfg


def darkConfig(useLamp):
    cfg = mkGosConfig()
    cfg.insert('box:uppercover', 'open')
    cfg.insert('box:lowercover', 'open')
    cfg.insert('level0:optic', 'dark')
    cfg.insert('level0:state')
    cfg.insert('level1:optic', 'clear')
    cfg.insert('level1:state')
    cfg.insert('level2:optic', 'clear')
    cfg.insert('level2:state')
    if useLamp:
        cfg.insert('level3:optic', 'lamp')
        cfg.insert('level3:state', 'off')
    else:
        cfg.insert('level3:optic', 'clear')
        cfg.insert('level3:state')
    return cfg


def clearConfig(useLamp):
    cfg = mkGosConfig()
    cfg.insert('box:uppercover', 'open')
    cfg.insert('box:lowercover', 'open')
    cfg.insert('level0:optic', params.getString('fieldStop'))
    cfg.insert('level0:state')
    cfg.insert('level1:optic', 'clear')
    cfg.insert('level1:state')
    cfg.insert('level2:optic', 'clear')
    cfg.insert('level2:state')
    if useLamp:
        cfg.insert('level3:optic', 'lamp')
        cfg.insert('level3:state', 'on')
    else:
        cfg.insert('level3:optic', 'clear')
        cfg.insert('level3:state')
    return cfg


def polConfig(p, r):
    cfg = mkGosConfig()
    cfg.insert('level2:optic', params.getString('polarizer'))
    cfg.insert('level2:state', str(p))

    if r is not None:
        cfg.insert('level1:optic', params.getString('retarder'))
        cfg.insert('level1:state', str(r))
    else:
        cfg.insert('level1:optic', 'clear')
        cfg.insert('level1:state')  # is this really needed

    return cfg
