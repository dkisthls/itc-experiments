# inst-name   = atst.ics.asi
# dsp-id      = HLS-TEST-ASI-OBSERVE
# obs-task    = observe
# description = HLS TEst DSP

__dsp.modifiable,*.*
atst.ics.asi.comment,20 second obs
atst.ics.asi.fail,false
atst.ics.asi.maxTime,25
atst.ics.asi.minTime,20
