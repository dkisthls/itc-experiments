## 
# Header parsed by the ScriptReader
# category    = atst.ocs.op
# name        = gain 
# id          = hls-test-no-tel-gain
# language    = jython
# description = An OP Script with no telescope control for HLS testing
# view        = no-view
## 

def doWork():
  icsTask = makeTaskGroup() # same task for every target
  ICSsetup(icsTask)
  ICSexecuteAndWait(icsTask)

