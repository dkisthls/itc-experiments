################################################################################



# inst-name    = atst.ics.visp
# dsp-id       = visp-observe.test_02
# obs-task     = observe
# description  = observe: intensity,630,396,854, velocity 0.5, mapsz 40
################################################################################
__dsp.modifiable,*
atst.ics.visp.comment,
atst.ics.visp.obsmode,intensity
atst.ics.visp.gratingpos,-65.3654
atst.ics.visp.gratingName,316/63
atst.ics.visp.slitWidth,0.0410
atst.ics.visp.slitVelocity,"0.5"
atst.ics.visp.mapStart,-20
atst.ics.visp.mapEnd,20
atst.ics.visp.mapRepeats,1
atst.ics.visp.vcc1expTime,1
atst.ics.visp.vcc2expTime,1
atst.ics.visp.vcc3expTime,1

atst.ics.visp.camera1enabled,true
atst.ics.visp.camera2enabled,true
#atst.ics.visp.camera2enabled,false
atst.ics.visp.camera3enabled,true
#atst.ics.visp.camera3enabled,false

atst.ics.visp.arm1pos,-3.32
atst.ics.visp.arm2pos,-7.5396
atst.ics.visp.arm3pos,-20.0786
atst.ics.visp.wavelength1,630.2051
atst.ics.visp.wavelength2,396.7941
atst.ics.visp.wavelength3,854.231

atst.ics.visp.vcc1_hw_binSize,1\,1
atst.ics.visp.vcc1_hw_win_numberOfROIs,2
atst.ics.visp.vcc1_hw_win_ROI_1,0\,0\,2560\,1000
atst.ics.visp.vcc1_hw_win_ROI_2,0\,1160\,2560\,1000
atst.ics.visp.vcc1_hw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc1_hw_win_ROI_4,0\,0\,0\,0
atst.ics.visp.vcc1_sw_binSize,1\,1
atst.ics.visp.vcc1_sw_win_roiType,horizontal
atst.ics.visp.vcc1_sw_win_numberOfROIs,1
atst.ics.visp.vcc1_sw_win_ROI_1,0\,0\,2560\,2000
atst.ics.visp.vcc1_sw_win_ROI_2,0\,0\,0\,0
atst.ics.visp.vcc1_sw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc1_sw_win_ROI_4,0\,0\,0\,0

atst.ics.visp.vcc2_hw_binSize,1\,1
atst.ics.visp.vcc2_hw_win_numberOfROIs,2
atst.ics.visp.vcc2_hw_win_ROI_1,0\,0\,2560\,1000
atst.ics.visp.vcc2_hw_win_ROI_2,0\,1160\,2560\,1000
atst.ics.visp.vcc2_hw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc2_hw_win_ROI_4,0\,0\,0\,0
atst.ics.visp.vcc2_sw_binSize,1\,1
atst.ics.visp.vcc2_sw_win_roiType,horizontal
atst.ics.visp.vcc2_sw_win_numberOfROIs,1
atst.ics.visp.vcc2_sw_win_ROI_1,0\,0\,2560\,2000
atst.ics.visp.vcc2_sw_win_ROI_2,0\,0\,0\,0
atst.ics.visp.vcc2_sw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc2_sw_win_ROI_4,0\,0\,0\,0

atst.ics.visp.vcc3_hw_binSize,1\,1
atst.ics.visp.vcc3_hw_win_numberOfROIs,2
atst.ics.visp.vcc3_hw_win_ROI_1,0\,0\,2560\,1000
atst.ics.visp.vcc3_hw_win_ROI_2,0\,1160\,2560\,1000
atst.ics.visp.vcc3_hw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc3_hw_win_ROI_4,0\,0\,0\,0
atst.ics.visp.vcc3_sw_binSize,1\,1
atst.ics.visp.vcc3_sw_win_roiType,horizontal
atst.ics.visp.vcc3_sw_win_numberOfROIs,1
atst.ics.visp.vcc3_sw_win_ROI_1,0\,0\,2560\,2000
atst.ics.visp.vcc3_sw_win_ROI_2,0\,0\,0\,0
atst.ics.visp.vcc3_sw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc3_sw_win_ROI_4,0\,0\,0\,0

