# inst-name   = atst.ics.asi
# dsp-id      = SIM-10-ASI-POLCAL
# obs-task    = observe
# description = SIM 10 pol cal IP

__dsp.modifiable,*.*
atst.ics.asi.comment,20 second obs
atst.ics.asi.fail,false
atst.ics.asi.maxTime,25
atst.ics.asi.minTime,20
