################################################################################
# inst-name    = atst.ics.cn
# dsp-id       = SIM-10-CN-5-POLCAL-2
# obs-task     = polcal
# description  = SIM 10 Pol cal IP
################################################################################

__dsp.modifiable,*.*
atst.ics.cn.dsp:camReadoutModeCI,FFRAME
atst.ics.cn.dsp:camReadoutModeSP,FFRAME
atst.ics.cn.dsp:camSetSameAsSP,false
atst.ics.cn.dsp:ciScanCoverage,Center
atst.ics.cn.dsp:ciScanPattern,None
atst.ics.cn.dsp:ciScanningFOV,Full
atst.ics.cn.dsp:cm1Position,Pellicle
atst.ics.cn.dsp:coAddCI,1
atst.ics.cn.dsp:coAddSP,1
atst.ics.cn.dsp:exposureTimeMsCI,300
atst.ics.cn.dsp:exposureTimeMsSP,300
atst.ics.cn.dsp:numMeasurementsCI,1
atst.ics.cn.dsp:numMeasurementsSP,1
atst.ics.cn.dsp:numModStates,8
atst.ics.cn.dsp:primaryFieldScanning,FOV
atst.ics.cn.dsp:roiPosYCI,0
atst.ics.cn.dsp:roiPosYSP,0
atst.ics.cn.dsp:roiSizeYCI,2048
atst.ics.cn.dsp:roiSizeYSP,2048
atst.ics.cn.dsp:secondaryFieldScanning,None
atst.ics.cn.dsp:slitType,52s
atst.ics.cn.dsp:spHorizScanPatternWay,LtoR
atst.ics.cn.dsp:spScanDirection_1,Horizontal
atst.ics.cn.dsp:spScanPattern,Raster
atst.ics.cn.dsp:spScanningFOV,Inner
atst.ics.cn.dsp:spStepSize,0.0\,0.0
atst.ics.cn.dsp:spVertScanPatternWay,TtoB
atst.ics.cn.dsp:spatialBinSizeCI,1\,1
atst.ics.cn.dsp:spatialBinSizeSP,1
atst.ics.cn.dsp:specBinSize,1
atst.ics.cn.dsp:telescopeFOV,2.8
atst.ics.cn.dspType,CI_SP_SIMULTANEOUS
atst.ics.cn.fo.pol.namedPos,In
atst.ics.cn.fo.pol.spinningMode,Stepped
atst.ics.cn.mc.ci.fw1,Open
atst.ics.cn.mc.ci.fw2,HeI
atst.ics.cn.mc.fo.fw,Open
atst.ics.cn.mc.sp.dispersion,2.3540190528274104E-4
atst.ics.cn.mc.sp.fw,HeI-FeXIII
atst.ics.cn.mc.sp.fw.wl,1083
atst.ics.cn.mc.sp.order,52
atst.ics.cn.mc.sp.sam,Off
