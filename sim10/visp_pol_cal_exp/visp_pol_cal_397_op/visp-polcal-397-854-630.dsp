################################################################################
# inst-name   = atst.ics.visp
# dsp-id      = SIM-10-VISP-POLCAL-397-854-630
# obs-task    = polcal
# description = SIM 10 pol cal IP
################################################################################

__dsp.modifiable,*.*

atst.ics.visp.obsmode,polarimetric

atst.ics.visp.gratingpos,-65.3584
atst.ics.visp.gratingName,316/63
atst.ics.visp.slitWidth,0.0410
atst.ics.visp.camera1enabled,true
atst.ics.visp.arm1pos,-3.30856
atst.ics.visp.vcc1expTime,6.0
atst.ics.visp.wavelength1,630.20016
atst.ics.visp.camera2enabled,true
atst.ics.visp.arm2pos,-7.52432
atst.ics.visp.vcc2expTime,20.000000
atst.ics.visp.wavelength2,396.80008
atst.ics.visp.camera3enabled,true
atst.ics.visp.arm3pos,-20.0722
atst.ics.visp.vcc3expTime,20.000000
atst.ics.visp.wavelength3,854.20020

atst.ics.visp.numModStates,10
atst.ics.visp.numModCycles,25
atst.ics.visp.slitStepSz,0
atst.ics.visp.slitSteps,1
atst.ics.visp.mapStart,0
atst.ics.visp.mapRepeats,1

atst.ics.visp.vcc1_hw_binSize,1\,1
atst.ics.visp.vcc1_hw_win_numberOfROIs,2
atst.ics.visp.vcc1_hw_win_ROI_1,0\,0\,2560\,1000
atst.ics.visp.vcc1_hw_win_ROI_2,0\,1160\,2560\,1000
atst.ics.visp.vcc1_hw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc1_hw_win_ROI_4,0\,0\,0\,0
atst.ics.visp.vcc1_sw_binSize,1\,1
atst.ics.visp.vcc1_sw_win_roiType,horizontal
atst.ics.visp.vcc1_sw_win_numberOfROIs,1
atst.ics.visp.vcc1_sw_win_ROI_1,0\,0\,2560\,2000
atst.ics.visp.vcc1_sw_win_ROI_2,0\,0\,0\,0
atst.ics.visp.vcc1_sw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc1_sw_win_ROI_4,0\,0\,0\,0

atst.ics.visp.vcc2_hw_binSize,1\,1
atst.ics.visp.vcc2_hw_win_numberOfROIs,2
atst.ics.visp.vcc2_hw_win_ROI_1,0\,0\,2560\,1000
atst.ics.visp.vcc2_hw_win_ROI_2,0\,1160\,2560\,1000
atst.ics.visp.vcc2_hw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc2_hw_win_ROI_4,0\,0\,0\,0
atst.ics.visp.vcc2_sw_binSize,1\,1
atst.ics.visp.vcc2_sw_win_roiType,horizontal
atst.ics.visp.vcc2_sw_win_numberOfROIs,1
atst.ics.visp.vcc2_sw_win_ROI_1,0\,0\,2560\,2000
atst.ics.visp.vcc2_sw_win_ROI_2,0\,0\,0\,0
atst.ics.visp.vcc2_sw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc2_sw_win_ROI_4,0\,0\,0\,0

atst.ics.visp.vcc3_hw_binSize,1\,1
atst.ics.visp.vcc3_hw_win_numberOfROIs,2
atst.ics.visp.vcc3_hw_win_ROI_1,0\,0\,2560\,1000
atst.ics.visp.vcc3_hw_win_ROI_2,0\,1160\,2560\,1000
atst.ics.visp.vcc3_hw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc3_hw_win_ROI_4,0\,0\,0\,0
atst.ics.visp.vcc3_sw_binSize,1\,1
atst.ics.visp.vcc3_sw_win_roiType,horizontal
atst.ics.visp.vcc3_sw_win_numberOfROIs,1
atst.ics.visp.vcc3_sw_win_ROI_1,0\,0\,2560\,2000
atst.ics.visp.vcc3_sw_win_ROI_2,0\,0\,0\,0
atst.ics.visp.vcc3_sw_win_ROI_3,0\,0\,0\,0
atst.ics.visp.vcc3_sw_win_ROI_4,0\,0\,0\,0

